| Programming the HELLO.TEMP.45 board from the ARDUINO IDE |
| :- |

&nbsp;
&nbsp;

| 1) |
| :- |
| In Arduino IDE, go to `Preferences` |
| ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/FABACADEMY_HELLO-TEMP-45_ARDUINO-IDE00001.png) |

| 2) |
| :- |
| Locate the `Additional Boards Manager URL's` field (in blue on the picture below) |
| ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/FABACADEMY_HELLO-TEMP-45_ARDUINO-IDE00002.png) |

| 3) |
| :- |
| Into the `Additional Board Manager URL`, paste `https://raw.githubusercontent.com/damellis/attiny/ide-1.6.x-boards-manager/package_damellis_attiny_index.json` |
| ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/FABACADEMY_HELLO-TEMP-45_ARDUINO-IDE00003.png) |

| 4) |
| :- |
| Go to `Tools` --> `Boards` --> `Boards Manager` |
| ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/FABACADEMY_HELLO-TEMP-45_ARDUINO-IDE00004.png) |

| 5) |
| :- |
| Into the `Board Manager` research field, type in `ATtiny` and install the `attiny` package from David A. Mellis |
| ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/FABACADEMY_HELLO-TEMP-45_ARDUINO-IDE00005.png) |

| 6) |
| :- |
| Go to `Tools` --> `Boards`--> `ATtinys Microcontrollers` and select `ATtiny25/45/85` |
| ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/FABACADEMY_HELLO-TEMP-45_ARDUINO-IDE00006.png) |

| 7) |
| :- |
| Go to `Tools` --> `Processor` and select `ATtiny45` |
| ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/FABACADEMY_HELLO-TEMP-45_ARDUINO-IDE00007.png) |

| 8) |
| :- |
| Go to `Tools` --> `Clock` and select `Internal 8 MHz` |
| ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/FABACADEMY_HELLO-TEMP-45_ARDUINO-IDE00008.png) |

| 9) |
| :- |
| Go to `Tools` --> `Port` and select your port |
| ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/FABACADEMY_HELLO-TEMP-45_ARDUINO-IDE00009.png) |

| 10) |
| :- |
| Go to `Tools` --> `Programmer` and select `USBtinyISP` |
| ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/FABACADEMY_HELLO-TEMP-45_ARDUINO-IDE00010.png) |

| 11) |
| :- |
| Go to http://academy.cba.mit.edu/classes/input_devices/temp/hello.temp.45.cInto, copy the code, and paste it into a new (and empty) Arduino Sketch |

| 12 |
| :- |
| Connect your FABISP and your HELLO.TEMP.45 sensor by adding jumper wires to the 6 pins as shown below |
| ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/IMG_0282.png) |

| ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/FABACADEMY_HELLO-TEMP-45_ARDUINO-IDE00030_copy.jpg) |
| :-: |

| 13 | Connect your FABISP to your computer via a USB-A - USB-Mini cable | - |
| - | - | - |
| ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/IMG_0288.png) | ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/IMG_0289.png) | ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/IMG_0291.png) |

| 14 | Connect your HELLO.TEMP.45 sensor to your computer/source of power via an FTDI cable |
| - | - |
| ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/IMG_0285.png) | ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/IMG_0287.png) |

| 15 |
| :- |
| Hit the `UPLOAD` button on the ARDUINO IDE window where you have pasted the code to send the program to the ATtiny45 located on your sensor |
| PIX TO COME |


&nbsp;
&nbsp;
&nbsp;
&nbsp;


| FAB ACADEMY HELLO.TEMP.45 |
| :- |
| Once the programming of the sensor has been done, from your Terminal, you can use Python and TKINTER to visualize the temperature measured by the sensor. |
| Below is the list of commands you will have to type inside your Terminal window to start the visualization application. |

| - | ACTIONS AND COMMANDS TO VISUALIZE THE DATA COMING OUT OF THE HELLO.TEMP.45 SENSOR |
| :- | :- |
| 0) | Place the folder with the programming files you have downloaded from [HERE](http://academy.cba.mit.edu/classes/input_devices/index.html) on your computer's Desktop |
| 1) | Name the folder `FABLAB` |
| 1) | Open a Terminal window |  
| 2) | Inside the Terminal window type `cd Desktop/FABLAB` and press [ENTER] on your keyboard |
| 3) | You are are now inside the `FABLAB` folder with all the hello.temp.45 files |
| 4) | In your Terminal, type in the command `ls/dev/tty.usb` and press [ENTER] on your keyboard |
| 5) | Copy the result of the command `ls/dev/tty.usb`. On Mac it will look like this : `/dev/tty.usbserial-FTF53N0Z` |
| 6) | In your Terminal, type `sudo python hello.temp.45.py`|
| 7) | Hit the space-bar one time on your keyboard |
| 8 | Paste the results of the command `ls/dev/tty.usb` |
| 9) | You should get something like `sudo python hello.temp.45.py /dev/tty.usbserial-FTF53N0Z` |
| 10) | Press [ENTER] on your keyboard |
| 11) | A Python windows like the one below should open |
