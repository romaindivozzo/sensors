| How to connect and use the Troyka MQ4 Gas-Sensor with Arduino ? |
| - |
| ![](https://gitlab.com/romaindivozzo/troyka-mq4-arduino-mega/-/raw/master/IMG_0124.JPG)

| MQ4 Gas-Sensor Side | MQ4 Gas-Sensor Front | MQ4 Gas-Sensor Back |
| - | - | - |
| ![](https://gitlab.com/romaindivozzo/troyka-mq4-arduino-mega/-/raw/master/IMG_0126.JPG) | ![](https://gitlab.com/romaindivozzo/troyka-mq4-arduino-mega/-/raw/master/IMG_0125.JPG) | ![](https://gitlab.com/romaindivozzo/troyka-mq4-arduino-mega/-/raw/master/IMG_0127.JPG) |

---

| 1) | Physical Connections between ArduinoMega and MQ-4 |
| - | - |
| A | Connect MQ4's **`G`** pin to ArduinoMega's **`Ground`** pin (any of the three ground pins available should work) |
| B | Connect MQ4's **`V`** pin to ArduinoMega's **`5V`** pin (there is only one visible) |
| C | Connect MQ4's **`S`** pin to ArduinoMega's **`A4`** pin (thi is an analog **`input`** pin) |

---

| 2) | Install the MQUnifiedsensor Libraries from the Arduino IDE |
| - | - |
| D | Open the Arduino IDE (the Arduino app). |
| E | At the top menu, click on the **`Tools`** tab, then click on the **`Manage Libraries`** tab. A new window should open. This window is some kind of Arduino Libraries Browser. |
| F | From the research field on top of the new window/arduino libraries browser type **`MQUnifiedsensor`**. The library you need should show. There is an **`Install`** button. Click on it to install. When installed, it will show a green circle on the left of the window. Close the window. |
| G | Click on the **`File`** tab on the top menu, then click **`Examples`**, then click **`MQUnifiedsensors`**, then click on **`MQ-4 ALL`**. A new code window with block of code will open. |

---

| 3) | Configure the ArduinoMega, the Port and the Programmer in the Arduino IDE |
| - | - |
| H | From the top menu, click on **`Tools`** again, then click on **`Board`**, then chose the **`Arduino Atmega`** board from the menu. If you don't see it, you click on the **`Boards Manager`** tab to add it. |
| I | Connect your Arduino to your computer with a USB cable. |
| J | From the top menu, click on **`Tools`** again, then click on **`Port`**, then click on the port in the list that starts like **`dev/cu.usbmodem/AND-SOME-NUMBERS`**. |
| K | From the top menu, click on **`Tools`** again, then click on the **`Programmer`** tab and click on the tab that says **`ArduinoISP`**. |

---

| 4) | Upload the code from the Arduino IDE to the ArduinoMega Board |
| - | - |
| L | Go back to the Arduino Window with the code to program the MQ-4 Gas-Sensor. |
| M | Click on the **`Arrow Button`** to upload the code (what we call the "sketch"). |
| N | If things work, you should see a **`uploading bar`** on the down-right corner of the window. Once this bar disappears, it means that your sensor has started to sens. |

---

| 5) | Get the Data Output from your MQ-4 sensor |
| - | - |
| O | To see the data coming out of the gas-sensor, you click on the **`Magnifier Button`** at the top-right corner of the code-window. A new window should open. It is called the **`Serial Monitor`**. You should now see your data. |

---

| - | CO2 Values | CO2 Values while Blowing on Sensor | CO2 Values going back to normal (slowly) |
| - | - | - | - |
| P | ![Build Status](https://gitlab.com/romaindivozzo/troyka-mq4-arduino-mega/-/raw/master/Screenshot_2021-07-17_at_16.43.02.png) | ![Build Status](https://gitlab.com/romaindivozzo/troyka-mq4-arduino-mega/-/raw/master/Screenshot_2021-07-17_at_16.43.17.png) | ![Build Status](https://gitlab.com/romaindivozzo/troyka-mq4-arduino-mega/-/raw/master/Screenshot_2021-07-17_at_16.44.19.png) |

---
