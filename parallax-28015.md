| How to connect and use the Parallax 28015 ULTRASONIC RANGE FINDER Sensor with Arduino MEGA2560 |
| - |
| ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_PARALLAX-28015_ARDUINOMEGA.JPG)

| Parallax 28015 ULTRASONIC RANGE FINDER Sensor Front | Parallax 28015 ULTRASONIC RANGE FINDER Sensor Back |
| - | - |
| ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_PARALLAX-28015_FRONT.JPG) | ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_PARALLAX-28015_BACK.JPG) |

---

| 1) | Physical Connections between ArduinoMega and Parallax 28015 |
| - | - |
| A | Connect Parallax 28015's **`G`** pin to the ARDUINO MEGA's **`Ground`** pin (any of the three ground pins available should work) |
| B | Connect Parallax 28015's **`VIN`** pin to the ARDUINO MEGA's **`5V`** pin (there is only one visible) |
| C | Connect Parallax 28015's **`SIG`** pin to the ARDUINO MEGA's **`PWM7`** pin  |

---

| 2) | Sending the code to the ARDUINO MEGA |
| - | - |
| E | Open the Arduino IDE (the Arduino app). |
| F | Open a new window |
| G | Paste the following code instead of what's already there |

`Ping))) Sensor

  This sketch reads a PING))) ultrasonic rangefinder and returns the distance
  to the closest object in range. To do this, it sends a pulse to the sensor to
  initiate a reading, then listens for a pulse to return. The length of the
  returning pulse is proportional to the distance of the object from the sensor.

  The circuit:
  - +V connection of the PING))) attached to +5V
  - GND connection of the PING))) attached to ground
  - SIG connection of the PING))) attached to digital pin 7

  created 3 Nov 2008
  by David A. Mellis
  modified 30 Aug 2011
  by Tom Igoe

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/Ping
*/

// this constant won't change. It's the pin number of the sensor's output:
const int pingPin = 7;

void setup() {
  // initialize serial communication:
  Serial.begin(9600);
}

void loop() {
  // establish variables for duration of the ping, and the distance result
  // in inches and centimeters:
  long duration, inches, cm;

  // The PING))) is triggered by a HIGH pulse of 2 or more microseconds.
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
  pinMode(pingPin, OUTPUT);
  digitalWrite(pingPin, LOW);
  delayMicroseconds(2);
  digitalWrite(pingPin, HIGH);
  delayMicroseconds(5);
  digitalWrite(pingPin, LOW);

  // The same pin is used to read the signal from the PING))): a HIGH pulse
  // whose duration is the time (in microseconds) from the sending of the ping
  // to the reception of its echo off of an object.
  pinMode(pingPin, INPUT);
  duration = pulseIn(pingPin, HIGH);

  // convert the time into a distance
  inches = microsecondsToInches(duration);
  cm = microsecondsToCentimeters(duration);

  Serial.print(inches);
  Serial.print("in, ");
  Serial.print(cm);
  Serial.print("cm");
  Serial.println();

  delay(100);
}

long microsecondsToInches(long microseconds) {
  // According to Parallax's datasheet for the PING))), there are 73.746
  // microseconds per inch (i.e. sound travels at 1130 feet per second).
  // This gives the distance travelled by the ping, outbound and return,
  // so we divide by 2 to get the distance of the obstacle.
  // See: https://www.parallax.com/package/ping-ultrasonic-distance-sensor-downloads/
  return microseconds / 74 / 2;
}

long microsecondsToCentimeters(long microseconds) {
  // The speed of sound is 340 m/s or 29 microseconds per centimeter.
  // The ping travels out and back, so to find the distance of the object we
  // take half of the distance travelled.
  return microseconds / 29 / 2;
}
`
---

| 3) | Configure the ArduinoMega, the Port and the Programmer in the Arduino IDE |
| - | - |
| I | From the top menu, click on **`Tools`** again, then click on **`Board`**, then chose the **`Arduino Atmega`** board from the menu. If you don't see it, you click on the **`Boards Manager`** tab to add it. |
| J | Connect your Arduino to your computer with a USB cable. |
| K | From the top menu, click on **`Tools`** again, then click on **`Port`**, then click on the port in the list that starts like **`dev/cu.usbmodem/AND-SOME-NUMBERS`**. |
| L | From the top menu, click on **`Tools`** again, then click on the **`Programmer`** tab and click on the tab that says **`ArduinoISP`**. |

---

| 4) | Upload the code from the Arduino IDE to the ArduinoMega Board |
| - | - |
| M | Go back to the Arduino Window with the code to program the Parallax 28015 ULTRASONIC RANGE FINDER Sensor. |
| N | Click on the **`Arrow Button`** to upload the code (what we call the "sketch"). |
| O | If things work, you should see an **`uploading bar`** on the down-right corner of the window. Once this bar disappears, it means that your sensor has started to sens. |

---

| 5) | Get the Data Output from your Parallax 28015 sensor |
| - | - |
| P | To see the data coming out of the ULTRASONIC RANGE FINDER-sensor, you click on the **`Magnifier Button`** at the top-right corner of the code-window. A new window should open. It is called the **`Serial Monitor`**. You should now see your data. |
| Q | This sensor can measure your **`DISTANCES`**.

---
| R |
| :- |
| ULTRASONIC RANGE FINDER Code Sample |
| ![Build Status](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_PARALLAX-28015_1.png) |
| ULTRASONIC RANGE FINDER Values in Details : When I blow on the sensor, I can see the ULTRASONIC RANGE FINDER Value going up |
| ![Build Status](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_PARALLAX-28015_2.png) |


---
