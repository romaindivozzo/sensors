| How to connect and use the Adafruit VCNL4040 Proximity and Lux Sensor with Arduino UNO or Adafruit METRO 328P (same as arduino UNO) ? |
| :- |
| ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_ADAFRUIT_VCNL4040_METRO.JPG)

| Adafruit VCNL4040 Proximity and Lux Sensor Front | Adafruit VCNL4040 Proximity and Lux Sensor Back |
| - | - |
| ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_ADAFRUIT_VCNL4040_FRONT.JPG) | ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_ADAFRUIT_VCNL4040_BACK.JPG) |

---



| 1) | Physical Connections between Adafruit METRO (= Arduino UNO) and Adafruit VCNL4040 |
| :- | - |
| A | Connect Adafruit VCNL4040's **`G`** pin to the METRO's **`Ground`** pin (any of the three ground pins available should work) |
| B | Connect Adafruit VCNL4040's **`VIN`** pin to the METRO's **`5V`** pin (there is only one visible) |
| C | Connect Adafruit VCNL4040's **`SCL`** pin to the METRO's **`SCL`** pin (the last hole of the row after the AREF) |
| D | Connect Adafruit vcnl4040's **`SDA`** pin to the METRO's **`SDA`** pin (the penultimate hole of the row right after the AREF) |

---

| 2) | Install an VCNL4040 Libraries from the Arduino IDE |
| :- | - |
| E | **IMPORTANT** : the VCNL4040 library from Adafruit didn't work for me. **The VCNL4040 library from SPARKFUN worked right away**.
| F | Open the Arduino IDE (the Arduino app). |
| G | At the top menu, click on the **`Tools`** tab, then click on the **`Manage Libraries`** tab. A new window should open. This window is some kind of Arduino Libraries Browser. |
| H | From the research field on top of the new window/arduino libraries browser type **`SparkFun VCNL4040`**. The library you need should show. There is an **`Install`** button. Click on it to install. When installed, it will show a green circle on the left of the window. Close the window. |
| I | Click on the **`File`** tab on the top menu, then click **`Examples`**, then click on the **`SparkFun VCNL4040...`**, then click on the sketch you want to try. A new code window with blocks of code will open. |

---

| 3) | Configure the METRO ( = Arduino UNO), the Port and the Programmer in the Arduino IDE |
| - | - |
| J | From the top menu, click on **`Tools`** again, then click on **`Board`**, then chose the **`Arduino UNO`** board from the menu. If you don't see it, you click on the **`Boards Manager`** tab to add it. |
| K | Connect your METRO to your computer with a USB cable. |
| L | From the top menu, click on **`Tools`** again, then click on **`Port`**, then click on the port in the list that starts like **`dev/cu.usbmodem/AND-SOME-NUMBERS`**. |
| M | From the top menu, click on **`Tools`** again, then click on the **`Programmer`** tab and click on the tab that says **`ArduinoISP`**. |

---

| 4) | Upload the code from the Arduino IDE to the METRO Board |
| - | - |
| N | Go back to the Arduino Window with the code to program the Adafruit VCNL4040 Proximity and Lux Sensor. |
| O | Click on the **`Arrow Button`** to upload the code (what we call the "sketch"). |
| P | If things work, you should see an **`uploading bar`** on the down-right corner of the window. Once this bar disappears, it means that your sensor has started to sens. |

---

| 5) | Get the Data Output from your Adafruit VCNL4040 Sensor |
| - | - |
| Q | To see the data coming out of the Proximity and lux Sensor, you click on the **`Magnifier Button`** at the top-right corner of the code-window. A new window should open. It is called the **`Serial Monitor`**. You should now see your data. |
| R | As this sensor can measure distances, you might be interested in using the **`Serial-Plotter`** to get the results from the sensing operations.

---
| S |
| :- |
| PROXIMITY and LUX CODE SAMPLE. |
| ![Build Status](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_ADAFRUIT_VCNL4040_CODE.png) |
| IF I PUT MY HAND ON THE SENSOR AND MOVE IT 1 METER AWAY SUDDENLY, THE VALUE IN THE SERIAL MONITOR DROPS DOWN VERY QUICKLY FROM 5000 TO 0. |
| ![Build Status](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_ADAFRUIT_VCNL4040_SERIAL-MONITOR.png) |
| I REALLY RECOMMEND TRYING THE PLOTTER WHILE MOVING YOUR HAND IN FRON TOF THE PLOTTER. |
| ![Build Status](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_ADAFRUIT_VCNL4040_SERIAL-PLOTTER.png) |

---
