| How to connect and use the Adafruit AM2320 Humidity Sensor with Arduino MEGA2560 |
| - |
| ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_ADAFRUIT-AM2320_ARDUINOMEGA.JPG)

| Adafruit AM2320 Humidity Sensor Front | Adafruit AM2320 Humidity Sensor Back |
| - | - |
| ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_ADAFRUIT-AM2320_FRONT.JPG) | ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_ADAFRUIT-AM2320_BACK.JPG) |

---

| 1) | Physical Connections between ArduinoMega and Adafruit AM2320 |
| - | - |
| A | This time you will need a Breadboard to connect your sensor as you have to add x2 resistors to make the sensor work. |
| B | The resistors you'll need should be between 2.2K and 10K Ohm. |
| C | You will need 6 Male-to-Male jumper-wires. |
| D | The connections on the Adafruit METRO shown on the picture below are exactly the same on the Arduino Mega. No changes needed. |

---

| Wiring Adafruit AM2320 with an Arduino |
| - |
| ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_ADAFRUIT-AM2320_CONNECTIONS-ADA.png) |

---

| 2) | Install the AM2320_asukiaaa Library from the Arduino IDE |
| - | - |
| E | Open the Arduino IDE (the Arduino app). |
| F | At the top menu, click on the **`Tools`** tab, then click on the **`Manage Libraries`** tab. A new window should open. This window is some kind of Arduino Libraries Browser. |
| G | From the research field on top of the new window/arduino libraries browser type **`AM2320_asukiaaa`**. The library you need should show. There is an **`Install`** button. Click on it to install. When installed, it will show a green circle on the left of the window. Close the window. |
| H | Click on the **`File`** tab on the top menu, then click **`Examples`**, then click **`AM2320_asukiaaa`**, then click on the sketch you want to try. A new code window with blocks of code will open. |

---

| 3) | Configure the ArduinoMega, the Port and the Programmer in the Arduino IDE |
| - | - |
| I | From the top menu, click on **`Tools`** again, then click on **`Board`**, then chose the **`Arduino Atmega`** board from the menu. If you don't see it, you click on the **`Boards Manager`** tab to add it. |
| J | Connect your Arduino to your computer with a USB cable. |
| K | From the top menu, click on **`Tools`** again, then click on **`Port`**, then click on the port in the list that starts like **`dev/cu.usbmodem/AND-SOME-NUMBERS`**. |
| L | From the top menu, click on **`Tools`** again, then click on the **`Programmer`** tab and click on the tab that says **`ArduinoISP`**. |

---

| 4) | Upload the code from the Arduino IDE to the ArduinoMega Board |
| - | - |
| M | Go back to the Arduino Window with the code to program the Adafruit AM2320 Humidity Sensor. |
| N | Click on the **`Arrow Button`** to upload the code (what we call the "sketch"). |
| O | If things work, you should see an **`uploading bar`** on the down-right corner of the window. Once this bar disappears, it means that your sensor has started to sens. |

---

| 5) | Get the Data Output from your Adafruit AM2320 sensor |
| - | - |
| P | To see the data coming out of the Humidity-sensor, you click on the **`Magnifier Button`** at the top-right corner of the code-window. A new window should open. It is called the **`Serial Monitor`**. You should now see your data. |
| Q | This sensor can measure your **`HUMIDITY`**.

---
| R |
| :- |
| HUMIDITY Code Sample |
| ![Build Status](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_ADAFRUIT-AM2320_1.png) |
| HUMIDITY Values in Details : When I blow on the sensor, I can see the Humidity Value going up |
| ![Build Status](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_ADAFRUIT-AM2320_2.png) |
| HUMIDITY doesn't show well into the Serial Plotter |
| ![Build Status](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_ADAFRUIT-AM2320_3.png) |
---
