| How to connect and use the Adafruit TCS34725 RGB-Color-Sensor with Arduino ? |
| - |
| ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_ADAFRUIT-TCS34275_RGB-COLOR_ARDUINO-MEGA.JPG)

| Adafruit TCS34725 RGB-Color-Sensor Front | Adafruit TCS34725 RGB-Color-Sensor Back |
| - | - |
| ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_ADAFRUIT-TCS34275_RGB-COLOR_FRONT.JPG) | ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_ADAFRUIT-TCS34275_RGB-COLOR_BACK.JPG) |

---

| 1) | Physical Connections between ArduinoMega and Adafruit TCS34725 |
| - | - |
| A | Connect Adafruit TCS34725's **`G`** pin to ArduinoMega's **`Ground`** pin (any of the three ground pins available should work) |
| B | Connect Adafruit TCS34725's **`VIN`** pin to ArduinoMega's **`5V`** pin (there is only one visible) |
| C | Connect Adafruit TCS34725's **`SCL`** pin to ArduinoMega's **`SCL`** pin (the last hole of the row after the AREF) |
| D | Connect Adafruit TCS34725's **`SDA`** pin to ArduinoMega's **`SDA`** pin (the penultimate hole of the row right after the AREF) |

---

| 2) | Install the TCS34725 Libraries from the Arduino IDE |
| - | - |
| E | Open the Arduino IDE (the Arduino app). |
| F | At the top menu, click on the **`Tools`** tab, then click on the **`Manage Libraries`** tab. A new window should open. This window is some kind of Arduino Libraries Browser. |
| G | From the research field on top of the new window/arduino libraries browser type **`TCS34725`**. The library you need should show. There is an **`Install`** button. Click on it to install. When installed, it will show a green circle on the left of the window. Close the window. |
| H | Click on the **`File`** tab on the top menu, then click **`Examples`**, then click **`DFRobot_TCS34725`**, then click on the sketch you want to try (COLORVIEW in this case). A new code window with blocks of code will open. |

---

| 3) | Configure the ArduinoMega, the Port and the Programmer in the Arduino IDE |
| - | - |
| I | From the top menu, click on **`Tools`** again, then click on **`Board`**, then chose the **`Arduino Atmega`** board from the menu. If you don't see it, you click on the **`Boards Manager`** tab to add it. |
| J | Connect your Arduino to your computer with a USB cable. |
| K | From the top menu, click on **`Tools`** again, then click on **`Port`**, then click on the port in the list that starts like **`dev/cu.usbmodem/AND-SOME-NUMBERS`**. |
| L | From the top menu, click on **`Tools`** again, then click on the **`Programmer`** tab and click on the tab that says **`ArduinoISP`**. |

---

| 4) | Upload the code from the Arduino IDE to the ArduinoMega Board |
| - | - |
| M | Go back to the Arduino Window with the code to program the Adafruit TCS34725 RGB-color-Sensor. |
| N | Click on the **`Arrow Button`** to upload the code (what we call the "sketch"). |
| O | If things work, you should see an **`uploading bar`** on the down-right corner of the window. Once this bar disappears, it means that your sensor has started to sens. |

---

| 5) | Get the Data Output from your Adafruit TCS34725 sensor |
| - | - |
| P | To see the data coming out of the RGB-color-sensor, you click on the **`Magnifier Button`** at the top-right corner of the code-window. A new window should open. It is called the **`Serial Monitor`**. You should now see your data. |
| Q | As this sensor can measure the RGB Colors, you might be interested in using the **`Serial-Plotter`** to get the results from the sensing operations.

---
| R |
| :- |
| RGB COLOR CODE SAMPLE. |
| ![Build Status](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_ADAFRUIT-TCS34275_RGB-CODE.png) |
| RGB COLOR CAPTURED ON AN OBJECT (RED COLOR OF AN OBJECT) AT A GIVEN TIME. |
| ![Build Status](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_ADAFRUIT-TCS34275_RGB-COLOR-OUTPUT_AT-A-GIVEN-TIME.png) |
| IF THE RGB VALUES FROM THE THE SERIAL MONITOR ARE HIGHER THAN THE RGB STANDARD VALUES ON THE WEBPAGE, REMOVE THE LAST DIGITS SO AS TO KEEP THE VALUE UNDER 255. |
| ![Build Status](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_ADAFRUIT-TCS34275_RGB-COLOR-COMPARISON.png) |
| COMPARISON BETWEEN THE WEB RGB VISUALIZER AND THE OBJECT THE COLOR WAS CAPTURED FROM. |
| ![Build Status](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_ADAFRUIT-TCS34275_RGB-COLOR_COMPARISON.JPG) |


---
