# Summary

<!--* [Introduction](README.md)-->
* [ADAFRUIT-TCS34725 (COLOR)](adafruit-tcs34725.md)
* [ADAFRUIT-VCNL4040 (PROXIMITY)](adafruit-vcnl4040.md)
* [ADAFRUIT-AM2320 (HUMIDITY)](adafruit-am2320.md)
* [PARALLAX 28015 (ULTRASONIC RANGE FINDER)](parallax-28015.md)
* [SPARKFUN-MAX30105 (PARTICLE)](sparkfun-max30105.md)
* [SPARKFUN CCS811 (AIR QUALITY)](sparkfun-ccs811.md)
* [TROYKA-MQ4 (GAZ)](troyka-mq4.md)
* [FAB ACADEMY HELLO.TEMP.SENSOR](fab_academy_hello-temp-45.md)
* [SEEED STUDIO XIAO ESP32C3 (WEBSERVER)](xiao-esp32c3-webserver.md)
