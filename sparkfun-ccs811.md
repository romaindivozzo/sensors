| How to connect and use the Sparkfun CCS811 Air Quality-Sensor with Arduino ? |
| - |
| ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_SPARKFUN_CCS811_1.JPG)

| Sparkfun CCS811 Air Quality-Sensor Front | Sparkfun CCS811 Air Quality-Sensor Back |
| - | - |
| ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_SPARKFUN_CCS811_2.JPG) | ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_SPARKFUN_CCS811_3.JPG) |

---

| 1) | Physical Connections between METRO/UNO and Sparkfun CCS811 |
| - | - |
| A | Connect Sparkfun CCS811's **`G`** pin to METRO/UNO's **`Ground`** pin (any of the three ground pins available should work) |
| B | Connect Sparkfun CCS811's **`V`** pin to METRO/UNO's **`5V`** pin (there is only one visible) |
| C | Connect Sparkfun CCS811's **`SCL`** pin to METRO/UNO's **`SCL`** pin (the last hole of the row after the AREF) |
| D | Connect Sparkfun CCS811's **`SDA`** pin to METRO/UNO's **`SDA`** pin (the penultimate hole of the row right after the AREF) |

---

| 2) | Install the CCS811 Libraries from the Arduino IDE |
| - | - |
| E | Open the Arduino IDE (the Arduino app). |
| F | At the top menu, click on the **`Tools`** tab, then click on the **`Manage Libraries`** tab. A new window should open. This window is some kind of Arduino Libraries Browser. |
| G | From the research field on top of the new window/arduino libraries browser type **`CCS811`**. The library you need should show. There is an **`Install`** button. Click on it to install. When installed, it will show a green circle on the left of the window. Close the window. |
| H | Click on the **`File`** tab on the top menu, then click **`Examples`**, then click **`SPARKFUN MAX3010x Pulse and Proximity Sensor Library`**, then click on the sketch you want to try. A new code window with blocks of code will open. |

---

| 3) | Configure the METRO/UNO, the Port and the Programmer in the Arduino IDE |
| - | - |
| I | From the top menu, click on **`Tools`** again, then click on **`Board`**, then chose the **`Arduino Atmega`** board from the menu. If you don't see it, you click on the **`Boards Manager`** tab to add it. |
| J | Connect your Arduino to your computer with a USB cable. |
| K | From the top menu, click on **`Tools`** again, then click on **`Port`**, then click on the port in the list that starts like **`dev/cu.usbmodem/AND-SOME-NUMBERS`**. |
| L | From the top menu, click on **`Tools`** again, then click on the **`Programmer`** tab and click on the tab that says **`ArduinoISP`**. |

---

| 4) | Upload the code from the Arduino IDE to the METRO/UNO Board |
| - | - |
| M | Go back to the Arduino Window with the code to program the Sparkfun CCS811 Air Quality-Sensor. |
| N | Click on the **`Arrow Button`** to upload the code (what we call the "sketch"). |
| O | If things work, you should see an **`uploading bar`** on the down-right corner of the window. Once this bar disappears, it means that your sensor has started to sens. |

---

| 5) | Get the Data Output from your Sparkfun CCS811 sensor |
| - | - |
| P | To see the data coming out of the Air Quality-sensor, you click on the **`Magnifier Button`** at the top-right corner of the code-window. A new window should open. It is called the **`Serial Monitor`**. You should now see your data. |

---
| Q |
| :- |
| Air Quality Code Sample |
| ![Build Status](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_SPARKFUN_CCS811_4.png) |
| Air Quality Values in Details and Variations when I blow on it. |
| ![Build Status](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_SPARKFUN_CCS811_5.png) |

---
