| How to turn SEEED STUDIO XIAO ESP32C3 into a WEBSERVER on your phone |
| - |
| ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_XIAOESP32C3_WEBSERVER_0.jpeg) |
| ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_XIAOESP32C3_WEBSERVER_1.png) |
| ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_XIAOESP32C3_WEBSERVER_2.png) |
| ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_XIAOESP32C3_WEBSERVER_3.png) |
| ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_XIAOESP32C3_WEBSERVER_4.png) |
| ![](https://gitlab.com/romaindivozzo/sensors/-/raw/master/ROM_XIAOESP32C3_WEBSERVER_5.PNG) |


<!--
---

| 1) | Physical Connections between ArduinoMega and Adafruit AM2320 |
| - | - |
| A | This time you will need a Breadboard to connect your sensor as you have to add x2 resistors to make the sensor work. |
| B | The resistors you'll need should be between 2.2K and 10K Ohm. |
| C | You will need 6 Male-to-Male jumper-wires. |
| D | The connections on the Adafruit METRO shown on the picture below are exactly the same on the Arduino Mega. No changes needed. |

---
